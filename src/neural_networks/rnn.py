import torch.nn as nn


class RNN(nn.Module):
    def __init__(self, n_features, n_hidden, n_classes):
        super(RNN, self).__init__()

        self.__flatten = nn.Flatten()

        self.__network = nn.Sequential(
            nn.Linear(n_features, n_hidden),
            nn.ReLU(),
            nn.Linear(n_hidden, n_classes)
        )

    def forward(self, input):
        return self.__network(self.__flatten(input))


class RNNModifiedSigmoid(nn.Module):
    def __init__(self, n_features, n_hidden, n_classes):
        super(RNNModifiedSigmoid, self).__init__()

        self.__flatten = nn.Flatten()

        self.__network = nn.Sequential(
            nn.Linear(n_features, n_hidden),
            nn.ReLU(),
            nn.Linear(n_hidden, n_classes),
            nn.Sigmoid()
        )

    def forward(self, input):
        return self.__network(self.__flatten(input))


class RNNModifiedHeavy(nn.Module):
    def __init__(self, n_features, n_hidden, n_classes):
        super(RNNModifiedHeavy, self).__init__()

        self.__flatten = nn.Flatten()

        self.__network = nn.Sequential(
            nn.Linear(n_features, n_hidden),
            nn.ReLU(),
            nn.Linear(n_hidden, n_hidden),
            nn.ReLU(),
            nn.Linear(n_hidden, n_classes),
            nn.Sigmoid()
        )

    def forward(self, input):
        return self.__network(self.__flatten(input))

class RNNModifiedDropout(nn.Module):
    def __init__(self, n_features, n_hidden, n_classes):
        super(RNNModifiedDropout, self).__init__()

        self.__flatten = nn.Flatten()

        self.__network = nn.Sequential(
            nn.Linear(n_features, n_hidden),
            nn.ReLU(),
            nn.Dropout(p=.2),
            nn.Linear(n_hidden, n_hidden),
            nn.ReLU(),
            nn.Dropout(p=.2),
            nn.Linear(n_hidden, n_classes),
            nn.Sigmoid()
        )

    def forward(self, input):
        return self.__network(self.__flatten(input))
